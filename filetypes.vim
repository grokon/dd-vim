"##############################################################"
" vim: set foldmarker={,} foldlevel=0 foldmethod=marker :
"
" DD Vim Configuration
"
" Danilo Dellaquila K-Gigas Computers S.L.
"
" This is the personal Vim configuration of Danilo Dellaquila.
"
" File: filetype.vim
"
"##############################################################"

if exists("did_load_filetypes")
  finish
endif

augroup filetypedetect
  au! BufRead,BufNewFile *.zcml     set filetype=xml
  au! BufRead,BufNewFile *.rst      setfiletype rst
  au! BufRead,BufNewFile *.txt      setfiletype rst
  au! BufRead,BufNewFile *.md       setfiletype markdown
  au! BufRead,BufNewFile *.wiki     setfiletype moin
  "au! BufNewFile,BufRead *.wiki     setfiletype Wikipedia
augroup END

autocmd FileType html  set tabstop=2|set shiftwidth=2
autocmd FileType xhtml set tabstop=2|set shiftwidth=2
autocmd FileType xml   set tabstop=2|set shiftwidth=2
